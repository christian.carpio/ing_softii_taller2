package labcodeinspection;

import java.util.Scanner;
import java.util.logging.Logger;
/**Comment **/
public final class EmailApp {
	/**Comment**/
	private static final Logger LOGGER = Logger.getLogger(EmailApp.class.getName());	



	/**Comment **/
	public static void main(String[] args) {
		
		try(Scanner scan = new Scanner(System.in)) {
		LOGGER.info("Enter your first name: ");
		final String firstName = scan.nextLine();

		LOGGER.info("Enter your last name: ");
		final String lastName = scan.nextLine();

		LOGGER.info("\nDEPARTMENT CODE\n1. for sales\n2. for Development\n3. for accounting\nEnter code: ");

		final int depChoice = scan.nextInt();
		
		final Email email = new Email(firstName, lastName);
		email.setDeparment(depChoice);
		email.generateEmail();
		email.showInfo();
		}



	}
	
	private EmailApp() {
        throw new AssertionError("Utility class EmailApp should not be instantiated");
    }
	

}
