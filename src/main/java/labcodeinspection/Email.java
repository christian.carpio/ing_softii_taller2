package labcodeinspection;

import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

/**Class **/
public class Email { //Class

	/**Comment **/
	private final String firstName; //transient
	/**Comment **/
	private final String lastName;
	/**Comment **/
	private String password;
	/**Comment **/
	private String department;
	/**Comment **/
	private final static int DEFAULT_LENGHT_1 = 8;
	/**Comment **/
	private String emailName;

	/**Comment **/
	private static final Logger LOGGER = Logger.getLogger(Email.class.getName());	
	/**Comment **/
	public Email(final String firstName, final String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	/**Comment **/
	public void showInfo() {
	
		 if (LOGGER.isLoggable(Level.INFO)) {
		        LOGGER.info("\nFIRST NAME= " + firstName + "\nLAST NAME= " + lastName);
		        LOGGER.info("DEPARTMENT= " + department + "\nEMAIL= " + emailName + "\nPASSWORD= " + password);
		    }
	}

	/**Comment **/
	public void setDeparment(final int depChoice) {
		switch (depChoice) {
		case 1:
			this.department = "sales";
			break;
		case 2:
			this.department = "dev";
			break;
		case 3:
			this.department = "acct";
			break;
		default:
			this.department = "sales";
			break;
		}
	}

	private String randomPassword(final int length) {
		final String set = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890#$&@*";
		final char[] password = new char[length];
		for (int i = 0; i < length; i++) {
			final int rand = (int) (Math.random() * set.length());
			password[i] = set.charAt(rand);
		}
		return new String(password);
	}

	/**Comment **/
	public void generateEmail() {
		String localMail;
		this.password = this.randomPassword(this.DEFAULT_LENGHT_1);
		localMail = this.firstName.toLowerCase(Locale.ROOT) + this.lastName.toLowerCase(Locale.ROOT) + "@" + this.department
				+ ".espol.edu.ec";
		this.emailName = localMail;
	}
}
